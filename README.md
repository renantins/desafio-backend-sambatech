# Instruções de utilização

`./mvwn` Comando para executar a aplicação

`./mvnw test` Comando para executar os testes

`./mvnw package` Comando para empacotar a aplicação e distribuir

Documentação da API: http://localhost:8080/swagger-ui.html

Banco de dados: http://localhost:8080/h2-console 

package br.com.renan.sambatech.rest;

import br.com.renan.sambatech.domain.Midia;
import br.com.renan.sambatech.repository.MidiaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/medias")
public class MidiaController {


    private MidiaRepository midiaRepository;

    public MidiaController(MidiaRepository midiaRepository) {
        this.midiaRepository = midiaRepository;
    }

    @PostMapping
    public ResponseEntity<?> createMidia(@RequestBody Midia midia) throws URISyntaxException {
        Midia result = midiaRepository.save(midia);
        return ResponseEntity.created(new URI("/medias/" + result.getId()))
                .body(result);
    }

    @PutMapping
    public ResponseEntity<?> updateMidia(@RequestBody Midia midia) {
        if (midia.getId() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Campo Id vazio");
        }
        Midia result = midiaRepository.save(midia);
        return ResponseEntity.ok()
                .body(result);
    }

    @GetMapping
    public ResponseEntity<?> getAllMidias(@RequestParam(required = false) Boolean deletadas) {
        if(deletadas != null && deletadas){
            return ResponseEntity.ok().body(midiaRepository.findAll().stream().filter(midia -> midia.getDeleted() == true));
        }
        return ResponseEntity.ok().body(midiaRepository.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getMidia(@PathVariable Long id) {
        if (midiaRepository.findById(id).isPresent()) {
            Midia midia = midiaRepository.findById(id).get();
            return ResponseEntity.ok().body(midia);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteMidia(@PathVariable Long id) {
        if (midiaRepository.findById(id).isPresent()) {
            midiaRepository.deleteById(id);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }

}

package br.com.renan.sambatech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SambatechApplication {

    public static void main(String[] args) {
        SpringApplication.run(SambatechApplication.class, args);
    }

}

package br.com.renan.sambatech.repository;

import br.com.renan.sambatech.domain.Midia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MidiaRepository extends JpaRepository<Midia, Long> {
}

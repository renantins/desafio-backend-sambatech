package br.com.renan.sambatech;

import br.com.renan.sambatech.domain.Midia;
import br.com.renan.sambatech.repository.MidiaRepository;
import br.com.renan.sambatech.rest.MidiaController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
class SambatechApplicationTests {

    @Autowired
    private MidiaRepository midiaRepository;
    private MockMvc restMidiaMockMvc;
    private Midia midia;
    DateTimeFormatter formatters = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    private static final Long DEFAULT_ID = 1l;
    private static final String DEFAULT_NOME = "Video Full HD";
    private static final String DEFAULT_NOME_UPDATE = "Video 4k";
    private static final String DEFAULT_URL = "teste.com/video";
    private static final String DEFAULT_URL_UPDATE = "teste.com/video4k";
    private static final Integer DEFAULT_DURACAO = 10;
    private static final LocalDate DEFAULT_DATAUPLOAD = LocalDate.now();
    private static final Boolean DEFAULT_DELETED = false;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MidiaController midiaController = new MidiaController(midiaRepository);
        this.restMidiaMockMvc = MockMvcBuilders.standaloneSetup(midiaController).build();
    }

    @BeforeEach
    public void initTest() {
        midia = new Midia();
        midia.setId(DEFAULT_ID);
        midia.setNome(DEFAULT_NOME);
        midia.setUrl(DEFAULT_URL);
        midia.setDuracao(DEFAULT_DURACAO);
        midia.setDataUpload(DEFAULT_DATAUPLOAD);
        midia.setDeleted(DEFAULT_DELETED);
    }

    @Test
    public void createMidia() throws Exception {
        midia.setDataUpload(null);
        restMidiaMockMvc.perform(post("/medias").contentType(MediaType.APPLICATION_JSON).content(this.convertToJson(midia))).andExpect(status().isCreated());
        List<Midia> midiaList = midiaRepository.findAll();
        Midia testMidia = midiaList.get(midiaList.size() - 1);
        assertThat(testMidia.getId()).isEqualTo(DEFAULT_ID);
        assertThat(testMidia.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testMidia.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testMidia.getDuracao()).isEqualTo(DEFAULT_DURACAO);
        assertThat(testMidia.getDeleted()).isEqualTo(DEFAULT_DELETED);
    }

    @Test
    public void updateMidia() throws Exception {
        midia.setDataUpload(null);

        midiaRepository.saveAndFlush(midia);

        Midia updateMidia = midiaRepository.findAll().get(0);
        updateMidia.setNome(DEFAULT_NOME_UPDATE);
        updateMidia.setUrl(DEFAULT_URL_UPDATE);
        restMidiaMockMvc.perform(put("/medias").contentType(MediaType.APPLICATION_JSON).content(this.convertToJson(updateMidia))).andExpect(status().isOk());
        List<Midia> midiaList = midiaRepository.findAll();
        Midia testMidia = midiaList.get(midiaList.size() - 1);
        assertThat(testMidia.getId()).isEqualTo(DEFAULT_ID);
        assertThat(testMidia.getNome()).isEqualTo(DEFAULT_NOME_UPDATE);
        assertThat(testMidia.getUrl()).isEqualTo(DEFAULT_URL_UPDATE);
        assertThat(testMidia.getDuracao()).isEqualTo(DEFAULT_DURACAO);
        assertThat(testMidia.getDeleted()).isEqualTo(DEFAULT_DELETED);
    }

    @Test
    public void getAllMidias() throws Exception {
        midiaRepository.saveAndFlush(midia);

        restMidiaMockMvc.perform(get("/medias"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(midia.getId().intValue())))
                .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
                .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL)))
                .andExpect(jsonPath("$.[*].duracao").value(hasItem(DEFAULT_DURACAO)))
                .andExpect(jsonPath("$.[*].dataUpload").value(hasItem(DEFAULT_DATAUPLOAD.format(formatters))))
                .andExpect(jsonPath("$.[*].deleted").value(hasItem(DEFAULT_DELETED)));
    }

    @Test
    public void getMidiaById() throws Exception {
        midiaRepository.saveAndFlush(midia);

        restMidiaMockMvc.perform(get("/medias/{id}", midia.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
        assertThat(midia.getId()).isEqualTo(DEFAULT_ID);
        assertThat(midia.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(midia.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(midia.getDuracao()).isEqualTo(DEFAULT_DURACAO);
        assertThat(midia.getDataUpload()).isEqualTo(DEFAULT_DATAUPLOAD);
        assertThat(midia.getDeleted()).isEqualTo(DEFAULT_DELETED);
    }


    @Test
    public void getMidiasDeletadas() throws Exception {
        this.midia.setDeleted(true);
        midiaRepository.saveAndFlush(midia);

        restMidiaMockMvc.perform(get("/medias").param("deletadas", "true"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(midia.getId().intValue())))
                .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
                .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL)))
                .andExpect(jsonPath("$.[*].duracao").value(hasItem(DEFAULT_DURACAO)))
                .andExpect(jsonPath("$.[*].dataUpload").value(hasItem(DEFAULT_DATAUPLOAD.format(formatters))))
                .andExpect(jsonPath("$.[*].deleted").value(hasItem(!DEFAULT_DELETED)));
    }

    @Test
    @Transactional
    public void deleteMidia() throws Exception {
        midiaRepository.saveAndFlush(midia);

        int databaseSize = midiaRepository.findAll().size();

        restMidiaMockMvc.perform(delete("/medias/{id}",midia.getId()))
                .andExpect(status().isOk());

        List<Midia> midiaList = midiaRepository.findAll();
        assertThat(midiaList).hasSize(databaseSize - 1);
    }

    public String convertToJson(Midia midia) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(midia);
        System.out.println(json);
        return json;
    }

}
